import React from 'react'
import ReactPlayer from 'react-player'

export const Videos = (props) => {
    return (
        <div className='video' >
            <ReactPlayer width='300px' height='170px'
            controls={true} url={props.url} />
        </div>
    )
}
