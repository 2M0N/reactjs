import "../TableDataProject/Main.css"
import data from '../TableDataProject/Data.json'
import { Thead } from "./Thead"
import { useState } from "react"
import { Tbody } from "./Tbody"
import { FormInp } from "./FormInp"
import {nanoid} from 'nanoid'
import { EditableRow } from "./EditableRow"

export const TableMain = () => {
const [info, setinfo] = useState(data)

const [editInfo, seteditInfo] = useState(null)

const [inpAdd, setinpAdd] = useState({
  fullName:'',
  address:'',
  phone:'',
  email:'',
})
const [editContact, seteditContact] = useState({
  fullName:'',
  address:'',
  phone:'',
  email:'',
})
// ===========================================
// On Table Form Change 
const onFromChang = (e)=> {
  e.preventDefault()

  const fieldName = e.target.getAttribute('name'); // getting Attribute name 
  const fieldValue = e.target.value;
  const newFormData = {...inpAdd};  // Creating a copy of inpAdd State
  newFormData[fieldName] = fieldValue

  setinpAdd(newFormData) // pushing new Object
}
// =========================================
// Table Form Submit
const onSubmitForm = (e) => {
  e.preventDefault()

  const newContact = {
    id : nanoid(),
    fullName: inpAdd.fullName,
    address: inpAdd.address,
    phone: inpAdd.phone,
    email: inpAdd.email,
  }

  const newContacts = [...info,newContact] // try passing inpAdd
  setinfo(newContacts)// Creating new array and adding with data
}
// ===============================================
// Edit Form Submit
const handleEditSubmit =(e)=> {
e.preventDefault()

const newInfo ={
  id : nanoid(),
    fullName: editContact.fullName,
    address: editContact.address,
    phone: editContact.phone,
    email: editContact.email,
    }

  const newinfo = [...info]
  const index = info.findIndex((contact) => contact.id === editInfo);
  newinfo[index] = newInfo 
  setinfo(newinfo)
  seteditInfo(null)
}
//==============================================
// Delete Button 
const deleteClick = (eID) => {
  const newinfo = [...info]

  const index = info.findIndex((info)=> info.id === eID);
  newinfo.splice(index,1) // To Delet an array
  setinfo(newinfo)
}
//=============================================
// Edit Button 
const editClick = (event,e) => {
  event.preventDefault()
  seteditInfo(e.id)

  const formValues = {
    fullName: e.fullName,
    address: e.address,
    phone: e.phone,
    email: e.email,
  };
  seteditContact(formValues)
}
// =========================================
// Undo Button 
const undoBtn = () => {
  seteditInfo(null)
}
//=============================================
// Handle Edit 
const handleEdit = (e) => {
e.preventDefault()

const fieldName = e.target.getAttribute("name")
const fieldValue = e.target.value

const newInfo = {...editContact}
newInfo[fieldName] = fieldValue

seteditContact(newInfo)
}
return (
    <div className='main'>
    <div className='first_container'> 
    <form onSubmit={handleEditSubmit}> 
    <table>
    <Thead/>
    <tbody>   
    {info.map(e => e.id === editInfo ?
    <EditableRow editContact={editContact}
    undoBtn={undoBtn} handleEdit={handleEdit} /> 
    :
    <Tbody deleteClick={deleteClick}
    editClick={editClick}
    key={e.id} e={e} /> )}
    </tbody>
    </table>
    </form>
    </div> 

    <FormInp 
    onFromChang={onFromChang}
    onSubmitForm={onSubmitForm} />
       
    </div>
)
}
