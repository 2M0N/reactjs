import React from 'react'

export const EditableRow = (props) => {
    const {editContact,undoBtn,
           handleEdit} = props
    return (
        <>
        <tr>
            <td>
                <input type='text'
                required='required'
                name='fullName'
                value={editContact.fullName}
                onChange={handleEdit} />
            </td>
            <td>
                <input type='text'
                required='required'
                name='address' 
                value={editContact.address}
                onChange={handleEdit} />
            </td>
            <td>
                <input type='text'
                required='required'
                name='phone' 
                value={editContact.phone}
                onChange={handleEdit} />
            </td>
            <td>
                <input type='text'
                required='required'
                name='email'
                value={editContact.email}
                onChange={handleEdit}  />
            </td>

            <td>
                <button onClick={undoBtn}
                className='undobtn'> Undo </button>
                <button type='submit'> Submit </button>
            </td>
        </tr>
        </>
    )
}
