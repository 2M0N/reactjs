import React from 'react'

export const FormInp = ({onFromChang,onSubmitForm}) => {
    return (
        <div className='input_sect'> 
        <h1> Add Contact </h1>
        <form className='showForm' 
        onSubmit={onSubmitForm}>
            <input type="text"
              name="fullName"
              required="required"
              placeholder="Enter a name..."
              onChange={onFromChang} />
    
            <input type="text"
              name="address"
              required="required"
              placeholder="Enter Address..."
              onChange={onFromChang} />
    
            <input type="text"
              name="phone"
              required="required"
              placeholder="Enter Phone..."
              onChange={onFromChang} />
    
              <input type="text"
              name="email"
              required="required"
              placeholder="Enter Email..."
              onChange={onFromChang} />
    
              <button type='submit'> Submit </button>
        </form>
        </div>
    )
}
