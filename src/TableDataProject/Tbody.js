import React from 'react'

export const Tbody = ({e,editClick,deleteClick}) => {
    return (
        <>
            <tr key={e.id}>
            <td> {e.fullName} </td>
            <td> {e.address} </td>
            <td> {e.phone} </td>
            <td> {e.email} </td>

            <td> 
                <button type='button'
                onClick={(event) => editClick(event,e)}>
                    Edit
                </button>

                <button className='deletebtn'
                type='button'
                onClick={() => deleteClick(e.id)}>
                    Delete
                </button>
            </td>
            </tr>
        </>
    )
}
