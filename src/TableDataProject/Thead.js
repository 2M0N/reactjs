import React from 'react'

export const Thead = () => {
    return (
        <><thead>
            <tr>
                <th> Name</th>
                <th> Address</th>
                <th> Phone Number</th>
                <th> Email</th>
                <th>Actions</th>
            </tr>
        </thead>
        </>
    )
}
