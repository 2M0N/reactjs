import React from 'react'
import { Video } from './Video'
import classes from './Css/Videos.module.css'

export const Videos = () => {
    return (
        <div className={classes.videos} >
            <Video />
            <Video />
            <Video />
            <Video />
            <Video />
            <Video />
            <Video />
            <Video />
        </div>
    )
}
