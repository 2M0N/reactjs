import classes from './Css/Illas.module.css'
import ilas from './Image/signup.svg'
export const Illas = () => {
    return (
        <div>
            <div className={classes.ilas_holder}>
            <img
            className={classes.ilas} src={ilas} alt='ilas' />
        </div>
        </div>
    )
}
