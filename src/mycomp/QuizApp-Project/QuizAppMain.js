import React from 'react'
import { Layout } from './Layout'
import { Quiz } from './Pages/Quiz'
// import { Home } from './Pages/Home'
// import { Login } from './Pages/Login'
// import { Signup } from './Pages/Signup'



export const QuizAppMain = () => {
  return (
    <>
    <Layout>
      {/* <Home /> */}
      {/* <Signup /> */}
      {/* <Login /> */}
      <Quiz />
    </Layout>
    </>
  )
}
