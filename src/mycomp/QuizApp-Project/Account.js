import React from 'react'
import classes from './Css/Nav.module.css'
import accountLogo from './Image/account-logo.png'


export const Account = () => {
  return (
    <div className={classes.account}>

        <img alt='account-Logo'
        className={classes.account_logo} src={accountLogo} /> 
      <span className={classes.span_signup}> <a href="/" > Sign up</a></span>
    </div>
  )
}
