import React from 'react'
import classes from './Css/Form.module.css'

export const Form = ({children}) => {
    return (
        <> <form className={classes.form}> 
           {children}
          </form>
        </>
    )
}
