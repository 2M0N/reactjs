import React from 'react'
import Tlogo from './Image/T-logo.png'
import './Css/Main.css'
import classes from './Css/Nav.module.css'
import { Account } from './Account'

export const Nav = () => {
    return (
        <div className='nav'>
            <ul className={classes.Nav_ul}>
                <li className='nav-li'> 
                    <a className={classes.a_tag} href='#/'> 
                        <img className={classes.tlogo} alt='T-logo' src={Tlogo} />
                        <div> Learn With Tumon</div>
                    </a>
                </li>
            </ul>


            <Account />
        </div>
    )
}
