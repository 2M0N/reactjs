import React from 'react'
import classes from './Css/FormSubmit.module.css'

export const FormSubmit = (props) => {
    return (
        <div className={classes.submit}>
            <span className={classes.span}> {props.text} </span>
        </div>
    )
}
