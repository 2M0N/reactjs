import React from 'react'
import { Nav } from './Nav'
import classes from './Css/Layout.module.css'

export const Layout = ({children}) => {
    return (
        <> 
        <div className={classes.body_hold}>
            <Nav />

            <main className={classes.main}> 
                <div className={classes.container}> 

                    {children}

                </div>
            </main>
        </div>
        </>
    )
}
