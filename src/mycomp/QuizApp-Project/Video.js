import React from 'react'
import thumb from './Image/thumb.png'
import classes from './Css/Video.module.css'

export const Video = () => {
    return (
        <>
            <a className={classes.video_a} href='/'>
                <div className={classes.videohold} >
                    <img alt='thubm'
                    className={classes.image} src={thumb} />
                    <p> #23 React Hooks Bangla - React useReducer hook Bangla </p>

                    <div className={classes.qmeta}>
                    <p>10 Questions</p>
                    <p>Score : Not taken yet</p>
                    </div>

                </div>
            </a>
        </>
    )
}
