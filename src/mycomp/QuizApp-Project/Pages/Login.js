import classes from '../Css/Login.module.css'
import { Form } from '../Form'
import { FormSubmit } from '../FormSubmit'
import { Illas } from '../Illas'
import { Textinputs } from '../Textinputs'


export const Login = () => {
    return (
        <>
        <h1> Login to your account </h1>
        <div className='column' > 
        <Illas />
        <Form className={classes.login}>
        <Textinputs type='text' placeholder='Enter Name' />
        <Textinputs type='password' placeholder='Enter Password' />

        <FormSubmit text='Submit' />
        <div className='info'>
            Already have an account? <a href="/">Login</a> instead.
            </div>
        </Form>

        </div>

       
     
        </>
    )
}
