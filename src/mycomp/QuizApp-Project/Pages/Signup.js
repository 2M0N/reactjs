import React from 'react'
import classes from '../Css/Signup.module.css'
import { Form } from '../Form'

import { Textinputs } from '../Textinputs'
import { CheckBox } from '../CheckBox'
import { FormSubmit } from '../FormSubmit'
import { Illas } from '../Illas'

export const Signup = () => {
    return (
    <>
    <h1 className='h1'> Create an Account </h1>

    <div className='column'>

    <Illas />

    <Form className={classes.Form}>
        
        <Textinputs placeholder='Type Name' type='text' />
        <Textinputs placeholder='Enter Email' type='text' />
        <Textinputs placeholder='Enter Password' type='password' />
        <Textinputs placeholder='Confirm Password' type='password' />

        <CheckBox text='I agree with all Terms & Conditions' />

        <FormSubmit text='Submit' />

        <div className={classes.info}>
        Already have an account? <a href="/">Login</a> instead.
        </div>
    </Form>
    </div>
    </>
    )
}
