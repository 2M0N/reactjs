export const CheckBox = ({text,...rest}) => {
    return (
        <>
            <label>
                <input type='checkbox' />
                <span> {text} </span>
            </label>
        </>
    )
}
