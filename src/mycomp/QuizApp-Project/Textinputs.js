import React from 'react'
import classes from './Css/Textinputs.module.css'

export const Textinputs = (props) => {
  
    return (
        <div>
            <input  className={classes.inp} {...props} />
        </div>
    )
}
