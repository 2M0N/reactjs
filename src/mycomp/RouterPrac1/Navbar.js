import { NavLink } from "react-router-dom"

export default function Navbar() {
    return (
        <>
            <nav>
                <ul>
                    <li> <NavLink exact activeStyle={{fontWeight:'bold',color:'red'}} to='/'>
                         Home </NavLink> </li>
                    <li> <NavLink exact activeStyle={{fontWeight:'bold',color:'red'}} to='/About'>
                         About </NavLink> </li>
                    <li> <NavLink exact activeStyle={{fontWeight:'bold',color:'red'}} to='/Services'> 
                         Services </NavLink> </li>
                    <li> <NavLink exact activeStyle={{fontWeight:'bold',color:'red'}} to='/Post/js'> 
                         Javascript </NavLink> </li>
                </ul>
            </nav>
        </>
    )
}
