import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import About from './About'
import Dashboard from './Dashboard'
import Error from './Error'
import Home from './Home'  
import Navbar from './Navbar'
import Posts from './Posts'
import Services from './Services'


export default function RouteHolder1() {
    
    const isLoggedIn = false;

    return (
       
        <Router>
            <Navbar />
            <Switch> 
            <Route exact path='/' component={Home} />
            <Route exact path='/About' component={About} />

            {/* <Route exact path='/Services' > <Services number='3' />  </Route> */}
            <Route exact path='/Services' render={()=> <Services number='3' />} />

            <Route exact path='/Post/:category' component={Posts} />


            <Route exact path='/Dashboard' component={Dashboard} />
            <Route exact path='/Login'>
                {isLoggedIn ? <Redirect to="/Dashboard" /> : <Home /> }
            </Route>


            {/* for err hndl and Switch must needed */}
            <Route exact component={Error} />
            </Switch>
        </Router>
        
    )
}
