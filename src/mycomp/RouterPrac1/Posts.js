import { useParams } from "react-router";


export default function Posts() {
    const params = useParams()
    console.log(params)
    return (
        <>
            <h1> This is Posts Page : {params.category} </h1>
        </>
    )
}
