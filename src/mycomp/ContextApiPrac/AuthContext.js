import React from 'react'

export const AUC = React.createContext()

export default function AuthContext({children}) {

    

    const [isLoggedin, setisLoggedin] = React.useState(true);

    const ChangeAuth = () => {
        setisLoggedin(
            !isLoggedin
            )
    }
    console.log()
    return (
        <div>
            <AUC.Provider value={{isLoggedin, auth:ChangeAuth}}>
                {children } 
            </AUC.Provider>
        </div>
    )
}
