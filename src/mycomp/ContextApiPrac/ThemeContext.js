import React from 'react'

export const UC = React.createContext();
export default function ThemeContext({children}) {

    const [is,setIs] = React.useState({
        isdark:true,

        light: {background:'red'},    
        dark: { background: 'rgb(43,43,43)'},}
        )
// ======= To Definde isdark =========
    const isdark = is.isdark

    const click = () => {
        setIs({...is,isdark:!isdark})  
    }
   

    return (
        <> 
            <UC.Provider value={{is,click}}>
             {children} 
            </UC.Provider> 
            </>
    )
}
