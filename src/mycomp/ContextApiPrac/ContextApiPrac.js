import React from 'react'
import { AUC } from './AuthContext'
import Condivtwo from './Condivtwo'
import './Contextapi.css'
import { UC } from './ThemeContext'

export default function ContextApiPrac() {
    
    const {is,click} = React.useContext(UC)
    const {isLoggedin,auth} = React.useContext(AUC)
    
    const theme = is.isdark? is.dark: is.light
    console.log(is)
    return (
        <div className='maindiv' style={{background:theme.background}}>
                
        <h1> Oka Academy </h1>

        <div className='secdiv'>
            
            <p className='pb'> About </p>
            <p className='pb'> Center </p>
            <p className='pb'> Main </p> <br></br>
            <button className='btn-dark '
             onClick={click}> Change Theme </button>
            <button className=' btn-dark ' 
             onClick={auth}> {isLoggedin ? 'Logged in' : 'Logged out'} </button>

                    
        </div>
        <Condivtwo></Condivtwo>
        </div>
        
        
    )
}

