import React, { Component } from 'react'
import { UC } from './GrandContext'

export default class ClassContext extends Component {

    themeStyle = (dark) =>{
        return {
            background: dark ? '#333' : '#3C3'
        }
    }

    render() {
        return (
            <>
            <UC.Consumer>
                {darkTheme=>{
                    return <div style={this.themeStyle(darkTheme)}> Class Theme</div>
                }}
            </UC.Consumer>
                
            </>
        )
    }
}
