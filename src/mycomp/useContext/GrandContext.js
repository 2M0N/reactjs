import React from 'react'
import FuncContext from './FuncContext';
import ClassContext from './ClassContext';



export const UC = React.createContext()

export default function GrandContext() {

  const [darkTheme, setDarktheme] = React.useState({f:'ff', l:'ll'});

  const changeTheme = () => {
    setDarktheme(prevDarktheme => !prevDarktheme) 
  
  }
    
  return (
    <>
    <UC.Provider value={darkTheme}>

      <button onClick={changeTheme}> Change Theme</button>

      <ClassContext></ClassContext>
      <FuncContext></FuncContext>

    </UC.Provider>
    {console.log('darkTheme is ', darkTheme)}
    </>
  )
}
