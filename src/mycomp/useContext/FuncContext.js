import React from 'react'
import { UC } from './GrandContext'

export default function FuncContext() {

    const darkTheme = React.useContext(UC)

    const themeStyle = {
        background: darkTheme ? 'green' : 'red'
        }
    console.log(darkTheme.f,darkTheme.l)
    return (
        <div style={themeStyle}>
            Function Theme
        </div>
    )
}
