import React, { useEffect, useState } from 'react'

export default function UseEffPracTwo() {

    const [click,setclick] = useState(0)
    const [input, setinput] = useState('')
    // ============================
    useEffect( () => {
        document.title = `Clicke ${click} Times`;
        console.log('UseEff round')
    },[click]
    )

    // ============================
    const onClickHandle = () => {
        setclick(p => p +1)
    }
    // ============================

    return (
        <div>
            <input value={input} onChange={(e)=>setinput(e.target.value)} />
            <button onClick={onClickHandle} className='btn-danger'> Click {click} </button>
        </div>
    )
}
