import React, { useEffect,useState } from 'react'
import './GithubApi.css'
import axios from 'axios';
export const GithubApi = () => {
        // Sep 2021
        const [users, setusers] = useState([])

        const getUsers = async () => {
        const res = await axios.get('https://api.github.com/users');
        setusers(await res.data)
        console.log({users})
        }

       useEffect(()=> {
               getUsers()
       },[])

       const datalist = users.map(e=>{
               return(
                <div className='cards'>
                <div className='image'> 
                <img src={e.avatar_url} width='144' />
                </div>
                <div className='parti'>
                <div className='name'><b> {e.login} </b></div>
                <div className='nametag'>
                <span > {e.html_url} </span> 
                </div>

                <div className='footer'>
                        <div className='article'>
                        <span> Articles </span>
                                <span className='num'>
                                {Math.floor(Math.random() * 100)}
                                </span>
                        </div>
                        <div className='follow'>
                        <span> Followers </span>
                                <span className='num'>
                                {Math.floor(Math.random() * 10000)} 
                                </span>
                        </div>
                </div>
                </div>
        </div>
               )
       })
    return (
        <div className='main'> 
        <div className='lagp'><div className='lag'> Get Github Api  </div></div>
        
        <div className='containar'> 
       {datalist}

        </div>
            
        </div>
    )
}
