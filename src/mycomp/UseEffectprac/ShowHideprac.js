import React, { useState } from 'react'
import UseEffPracTwo from './UseEffPracTwo'

export default function ShowHideprac() {

    const [show,setshow] = useState(true)
    return (
        <div>
            {show && <UseEffPracTwo />}

            <button 
            onClick={() => setshow(p=>!p)}> 
            {show ? 'Hide' : 'Show'} </button>
        </div>
    )
}
