import React, { useState } from 'react'
import { Basket } from './Basket'
import './Main.css'
import { TopCards } from './TopCards'

export const UC = React.createContext()

export const Main = () => {
    // Cart Data 
    const [cart, setcart] = useState([])

    // Calculation 
    const itemPrice = cart.reduce((a,c)=> a + c.price * c.kg, 0)
    const tax = itemPrice * 0.10;
    const shippingprice = itemPrice < 1000 ? 30 : 50 ;
    const totalPrice = itemPrice + tax + shippingprice ;



    console.log(cart)
    // Add to Cart Function  
    const addToCart = (item) => {
        // setcart([...cart,item])
        const exist = cart.find((c) => c.id === item.id) 
        if (exist === undefined) {
            setcart([...cart, {...item, kg: 1 }])
        } else {
         setcart(cart.map((e) => { // Understanding Map here
            return e.id === item.id ? {...e, kg: e.kg +1} : e ;
          }));
      } 
    }
    
    // Add and Remove Buttons on Cart
    const removeCart = (i) => {
       setcart(cart.filter((a,indx) => i !== indx))
    }

    const onPlus = (item) => {
        setcart(cart.map((e) => {
            return e.id === item.id ? {...e, kg: e.kg +1} : e ;
          }));
    }

    const onMinus = (item) => {
        setcart(cart.map(e => item.id === e.id && item.kg > 0 ?{...e, kg: e.kg -1} : e ))
    }


    return (
        <> 
        <UC.Provider 
        value={{addToCart:addToCart,
        cart:cart, removeCart:removeCart, onPlus:onPlus,
        onMinus:onMinus, itemPrice:itemPrice,
        tax:tax,shippingprice:shippingprice,totalPrice:totalPrice}}>

        <div className='all'> 
        <h1 className='heading'> Furit Market </h1> 
        <TopCards/>
        <Basket />

        </div>

        </UC.Provider>
        </>
    )
}
