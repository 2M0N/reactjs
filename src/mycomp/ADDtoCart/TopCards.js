
import Data from './Data'

import { MainCards } from './MainCards'


export const TopCards = () => {
    return (
        <div> 
            <div className='Main-Card'>
                
            {Data.map((item) => {
              return <MainCards item={item} key={item.id} />
            })}
            </div>
        </div>
    )
}
