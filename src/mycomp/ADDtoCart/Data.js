import olive from './olive.png'
import apple from './apple.png'
import papaya from './papaya.png'

const Data = [
        {
            id: 1 ,
            img: olive ,
            title: 'Olive' ,
            price: 110,
        },

        {
            id: 2 ,
            img: apple ,
            title: 'Apple' ,
            price: 170,
        },

        {
            id: 3 ,
            img: papaya ,
            title: 'Papaya' ,
            price: 90,
        },
    ]

export default Data;