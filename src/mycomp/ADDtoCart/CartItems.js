import React from 'react'
import { UC } from './Main'

export const CartItems = (props) => {
    const {item,i} = props

    // Use Context 
    const {removeCart,onPlus,onMinus,} = React.useContext(UC)

    return (
        <div>
            <div className='card-holder-cart-items'>
                
                <div className='imgNtag-cart'>   
                <img className='image-cart' alt='' src={item.img} />
                <div className='itemTitle-cart'> {item.title} </div></div>

                {/* Remove and Add Buttons  */}
                <div className='cart-button-hold'>
                <button className='cart-remove'
                onClick={()=> removeCart(i)}> Remove </button> <br/>
                <button className='cart-plus'
                onClick={()=> onPlus(item)}> + </button> 
                <button className='cart-minus'
                onClick={()=> onMinus(item)}> - </button>
                </div>
                
                {/* quantity and price  */}
                <div className='quantity-and-price'>
                <div className='cart-quantity'> {item.kg} Kilogram </div>
                <div className='cart-price'> ৳{item.price * item.kg} </div>
                </div> 
                {/* <div className='itemPrice-cart'> ${item.price} </div>  */}
                </div>
        </div>
    )
}
