import React from 'react'
import { useContext } from 'react'
import { CartItems } from './CartItems'

import { UC } from './Main'


export const Basket = () => { 

    // Use Context 
    const {cart, itemPrice,tax,shippingprice,
        totalPrice} = useContext(UC)

    // Cart to show or not 
    const carts = () => {
            if (cart.length >= 1) {
             return ( <> <div className='basket'>
            <div className='basketMain'> 
            {cart.map( (item,i) => <CartItems key={i} item={item} i={i}  />)}
            </div>
        </div>

         <div className='cartCalc'>
         <div> Item Price = {itemPrice} </div>
         <div> Total Tax = {tax.toFixed(0)} </div>
         <div> Shipping Fee = {shippingprice} </div> <hr/>
         <div className='total-amount'> Total Amount = {totalPrice}৳ </div>
     </div> </>
        )
            } else {
               return <h1 className='cartH1'> Your Cart is Empty...</h1>
            }
        }
    return (
        <>
        {/* {cart.length >= 1 ? <div className='basket'>
            <div className='basketMain'> 
            {cart.map( (item,i) => <CartItems key={i} item={item} i={i}  />)}
            </div>
        </div> : null} */}
       
        {carts()}
      
        </>
    )
}
