import React from 'react'
import { UC } from './Main';

export const MainCards = (props) => {
    const {item} = props

    // useContext Received here
    const {addToCart} = React.useContext(UC)
    
    return (
        <div>
            <div className='card-holder'>
                
                {/* Imgage and Title  */}
                <div className='imgNtag'>
                    <img className='image' alt='' src={item.img} />
                    <div className='itemTitle'> Fresh {item.title} </div>
                </div>

                {/* Add To Cart Button and Price  */}
                <div className='topCardFooter'>
                <button onClick={ () =>  addToCart(item)} className='addToCart'> Add to Cart </button> 
                <div className='itemPrice'> ৳ {item.price} </div> 
                </div>

                </div>
        </div>
    )
}
