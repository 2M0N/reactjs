import React,{useState,useEffect,useRef} from 'react'

export default function UseRef() {
    
    const [name, setname] = useState('')
    const inputref = useRef()
    
    // const ref = useRef(0)

    // useEffect(() => {
    //     ref.current = ref.current +=1
    // })
    const focus = () => {
        inputref.current.focus()

        //============ Alternative way ==========
        // document.querySelector('#he').focus()
    }


    return (
        <div>
            <input id='he' ref={inputref} value={name} 
            onChange={e=>setname(e.target.value)}/>
            <h2>My name is {name} </h2>
            {/* <h2> I rendered {ref.current} times! </h2> */}
            <button onClick={focus}> focus </button>
        </div>
    )
}
