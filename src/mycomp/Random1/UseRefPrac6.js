// Catch Previous Value with useRef! It's tricky
import React, { useState, useRef, useEffect } from 'react'
export default function UseRefPrac6() {
const [inputValue, setinputValue] = useState('');
const prevInputValue = useRef("");
useEffect(()=> {prevInputValue.current = inputValue;
console.log(`Ref ${prevInputValue.current}`)
},[inputValue]);console.log(`state ${inputValue}`)
return ( <div>{console.log(`Component Rendered`)}
<input type='text' value={inputValue}
onChange={e => setinputValue(e.target.value)} />
<h2>Current Value: {inputValue}</h2>
<h2>Previous Value: {prevInputValue.current}</h2></div>)}
