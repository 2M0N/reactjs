import React from 'react'
import BoxModel from './BoxModel';

export default function BoxInput() {
//==========================================

    const [inp,setinp] = React.useState({
        height:'100px',color:"",show:false
    })
    const height = inp.height;
    const color  = inp.color;
    const show  = inp.show;


//===========================================
          // onChange Functions
    const onHeight = (e) => {
        setinp({...inp,height:e.target.value})
    }

    const onColor = (e) => {
        setinp({...inp,color:e.target.value})
    }

//===========================================

    const onC = () => {
        setinp({...inp,show:true})
    }

    const dltShow = () => {
        setinp({...inp,show:false})
    }

    const mount = show? <BoxModel height={height} color={color}/>:""


    return (
        <> <h1> Creat a Box!</h1>
            <input name='height' onChange={onHeight}
            value={height} placeholder='Type Height' />
            <input type='color' name='color' onChange={onColor}
            value={color} placeholder='Type Color' /> <br></br>
            
            <button onClick={onC} > Click </button>
            <button onClick={dltShow} > Delete</button>

            {mount}
        </>
    )
}
