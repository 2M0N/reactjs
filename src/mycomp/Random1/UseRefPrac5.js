// UseRef doesn't cause Re Render
import React from 'react'
import { useEffect, useRef, useState } from 'react/cjs/react.development'

export default function UseRefPrac5() {
    const [inputValue, setinputValue] = useState('')
    const [counststate, setcounststate] = useState(0)
    const count = useRef(0)

    useEffect(()=>{
        count.current = count.current +1
    })
    return (
        <div>
            <input type='text' value={inputValue}
            onChange={e => setinputValue(e.target.value)} />

            <h1> Render Count: {count.current} </h1>
            
        </div>
    )
}
