import React,{useState,useEffect} from 'react'

export const UseEffect = () => {
    const [resType, setresType] = useState('posts')
    const [sunmoon, setsunmoon] = useState("Sun")

    const sunmoonChange = () => {
        setsunmoon({
           
    useEffect(()=>{
        console.log('UseEffect Changed!')
    },[resType])

    return (
        <>
            <button onClick={()=> 
                setresType('posts')}> posts </button>

            <button onClick={()=> 
                setresType('users')}> users </button>

            <button onClick={()=> 
                setresType('comments')}> comments </button>

            <h1> {resType} </h1>

            <button onClick={sunmoonChange}> {sunmoon} </button>
        </>
    )
}
