import React from 'react'
import { useEffect,useState } from 'react/cjs/react.development';

export default function UseEffprac5() {

	const [count, setcount] = useState(0);

	useEffect(()=> {
		let timer = setTimeout(()=>{
			setcount((e)=>e +1)
		},1000)
		return () => clearInterval(3000)
	})

	
	return (
		<>
			<h1> I've rendered {count} times! </h1>
		</>
	)
}