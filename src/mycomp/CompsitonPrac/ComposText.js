import React from 'react'

export default function ComposText({addEmoji}) {
    const text = " That is React"
    return (
        <div>
            {addEmoji ? addEmoji(text,'🤯') : text}
        </div>
    )
}
