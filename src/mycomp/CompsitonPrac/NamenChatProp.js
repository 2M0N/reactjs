import React from 'react'

export default function NamenChatProp(props) {
    return (
        <div className='namechapprop'> 
            <div className='split-left'>  &nbsp;
                {props.left}
            </div>

            <div className='split-right'>  &nbsp;
                {props.right}
            </div>

        </div>
    )
}
