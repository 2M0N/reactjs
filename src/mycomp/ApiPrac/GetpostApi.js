import React, { useEffect, useState } from 'react'
// Get Api Using useState  //
// It's not the best for big project.  //
// So, We should use useReducer insted //
export default function GetpostApi() {
    const [loading,setloading] = useState(true);
    const [post,setpost] = useState({});
    const [err,seterr] = useState();

    useEffect(()=>{
        fetch('https://jsonplaceholder.typicode.com/posts/1')
        .then((res)=> res.json())
        .then((json)=>{
            setloading(false);
            setpost(json);
            seterr('')
        })
        .catch((err)=>{
            setloading(false);
            setpost({})
            seterr("Error 404")
        });
        
    },[])
    console.log(post)
    
    return (
        <>
           {loading ? "Loading..." : post.body}
           {err } 
        </>
    )
}
