import React, { useEffect, useReducer } from "react";
const initialState = {
    loading: true,
    post: {},
    error: '',
}


const reducer = (state,action) => {
    switch (action.type) {
        case "SUCCESS": 
        return {
            loading: false,
            post: action.data,
            error: '',
        }
        
        case "ERROR":
            return {
                loading: false,
                post: {},
                error: 'Error 404',
            }
        default: return state; // try leaving noting 
    }
}

export default function GetpostUseReducer() {
    const [state, dispatch] = useReducer(reducer,initialState)

    useEffect( async ()=>{
    fetch('https://jsonplaceholder.typicode.com/posts/1')
    .then((response) => response.json())
    .then((json) => {
        dispatch({ type: 'SUCCESS', data: json });
    })
    .catch(() => {
        dispatch({ type: 'ERROR' });
    });
},[])
    return (
        <div onclick={} > 
            {state.loading ? "Loading..." : state.post.title}
           {state.error} {console.log(state)}
        <div/>
    )
}
