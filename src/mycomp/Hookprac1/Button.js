import React from 'react'

export default function Button({click,children}) {
    console.log(`Rendering ${children}`)
    return (
        <>
            <button onClick={click}> {children} </button>
        </>
    )
}
