import React, { useReducer } from 'react'
import ReduContex1 from './ReduContex1';

export const Context = React.createContext();

const ACTION = {
    INC:"Inc",
    DEC:"DEC"
}

const initialState ={
    count: 0,
};

const reducer = (state,dispatch) => {
    switch (dispatch) {
        case ACTION.INC : return {count:state.count +1};
        case ACTION.DEC : return {count:state.count -1};
        default: return state;
    }
}

export default function ReduWithContext() {

    const [state, dispatch] = useReducer(reducer, initialState)
    return (
        <> {console.dir(dispatch)}
            <h1> Count: {state.count} </h1>
            <br></br>
            <Context.Provider value={{action:ACTION,dis:dispatch}}>
                <ReduContex1 />
            </Context.Provider>
        </>
    )
}
