import React from 'react'

export default function ShowCount({count,title}) {

    console.log(`Rendering ${title}...`)
    return (
        <>
            {title} is {count}
        </>
    )
}
