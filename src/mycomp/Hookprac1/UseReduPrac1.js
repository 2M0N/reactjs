import React, { useReducer } from 'react'

const ACTION = {
    INC:"Inc",
    DEC:"DEC"
}

const initialState ={
    count: 0,
};

const reducer = (state,dispatch) => {
    switch (dispatch) {
        case ACTION.INC : return {count:state.count +1};
        case ACTION.DEC : return {count:state.count -1};
        default: return state;
    }
}

export default function UseReduPrac1() {

    const [state, dispatch] = useReducer(reducer, initialState)
    return (
        <> {console.log(typeof state)}
            <h1> Count: {state.count} </h1>
            <br></br>
            <button onClick={()=> dispatch(ACTION.INC)}> Inc.</button>
            <button onClick={()=> dispatch(ACTION.DEC)}> Dec.</button>
        </>
    )
}
