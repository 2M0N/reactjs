import React, { useEffect, useRef } from 'react'

export default function UseRefprac2() {
    const input = useRef(null)
    useEffect( () => {
       input.current.focus()
    },[])
    return (
        <>
            <input ref={input} type="text" />
        </>
    )
}
