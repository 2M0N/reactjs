import React, { useContext } from 'react'
import { Context } from './ReduWithContext'

export default function ReduContex2() {

    const UC = useContext(Context)
    
    return (
        <div> <br></br>
            <button onClick={()=> UC.dis(UC.action.INC)}> Inc.</button>
            <button onClick={()=> UC.dis(UC.action.DEC)}> Dec.</button>
        </div>
    )
}
