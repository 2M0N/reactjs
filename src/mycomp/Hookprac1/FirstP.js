import React, { useState, useMemo } from 'react'
import Button from './Button'
import ShowCount from './ShowCount'
import Title from './Title'

export default function FirstP() {

    const [count1,setcount1] = useState(0);
    const [count5,setcount5] = useState(0);


    const incOne = () => {
        setcount1(p => p +1)
    }

    const incFive = () => {
        setcount5(p => p +5)
    }

    const titleComp = useMemo(() => <Title/>, [])

    const sec1 = useMemo(() => { return (
        <div>
            <ShowCount count={count1} title={'count1'} />
            <Button click={incOne} > Inc One </Button> 
        </div> )
    }, [count1])

    const sec2 = useMemo(() => { return (
        <div>
            <ShowCount count={count5} title={'count5'} />
        <Button click={incFive} > Inc Five </Button> 
        </div> )
    }, [count5])

    return (
        <>
            {titleComp}
            {sec1}
            <hr></hr>
            {sec2}
        </>
    )
}
