import React, { useEffect, useState } from 'react'

export default function List({getItmes}) { 
    const [items, setitems] = useState([])
    useEffect(() => {
        setitems(getItmes())
        console.log('updating Items',)
    },[getItmes]) 
    
    return items.map((e) => <div key={e}> {e} </div>)
   
}