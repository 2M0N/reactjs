//UseCallback prac
import React, { useState } from 'react'
import { useCallback } from 'react/cjs/react.development'
import List from './List'


export const Prac1 = () => {
    const [number, setnumber] = useState(1)
    const [dark, setdark] = useState(true)

    const getItems =  useCallback( () => { 
        return [number, number +1, number +2,]
    },[number])  // Use Call Back is so Nice
console.log(getItems)
    const theme = {
        height: '100vh',
        backgroundColor : dark ? 'white' : 'grey',
        color : dark ? 'black' : 'white'
    }
    return (
        <div style={theme}> <br/> 
            <input type={'number'} value={number}
             onChange={(e)=> setnumber(parseInt(e.target.value))}  /> 

            <br/> <br/>
            
            <button onClick={()=> setdark(prevdark => !prevdark)}> Toggle Theme</button>
{/* Passing function */}
           <List getItmes={getItems} /> 
        </div>
    )
}
