import React from 'react'
import GoodsDiv from '/home/tumon/rapp/src/mycomp/JsonGoods/GoodsDiv.js'
import Search from './Search'

export const UC = React.createContext()

export default function Goodsmain() {

    const [obj,setObj] = React.useState(
    [{category: 'Sporting Goods', price: '$49.99',
    stocked: true, name: 'Football'},

   {category: 'Sporting Goods', price: '$9.99',
   stocked: true, name: 'Baseball'},

    {category: 'Sporting Goods', price: '$29.99',
        stocked: false, name: 'Basketball'},

    {category: 'Electronics', price: '$99.99',
        stocked: true, name: 'iPod Touch'},

    {category: 'Electronics', price: '$399.99',
        stocked: false, name: 'iPhone 5'},

    {category: 'Electronics', price: '$199.99',
        stocked: true, name: 'Nexus 7'}]
    )

    console.log()
    
    return (
        <div> 
            <UC.Provider value={obj} >
               <Search> </Search>
               <GoodsDiv> </GoodsDiv>

                
            </UC.Provider>
         
        </div>
    )
}
