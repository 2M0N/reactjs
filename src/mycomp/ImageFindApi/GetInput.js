import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import './Imageapi.css'

export default class GetInput extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             entry: '',
             m :{ marginTop: '7em',border:'none'}
        }
    }
    // ===== ON INPUT CHANGE  ==========

    onInputChange = (e) => {
        this.setState({entry: e.target.value})
    }

    // ===== ON FORM  SUBMIT ==========

    onform = (e) => {
        e.preventDefault()
        this.props.onSearch(this.state.entry)
        this.setState({m: {marginTop:'0em'}})
        
    }
    
    render() {
        // console.log(this.state.entry)
        const margin = this.state.m
        
        return (
            <>
            <form className='form' style={margin} onSubmit={this.onform}>

                <p className='id'> ID </p>
                
                <h1 className="h1 "> Pagla Google....</h1>

                <input className='input mt-4' onChange={this.onInputChange}
                value={this.state.entry} placeholder='  type....'/> <br></br>
                <Button variant="primary p-1 mt-2 btn-sm" className='btn' 
                onClick={this.onform}>Images</Button> <br></br> <br></br>
                <p className="pp float-end  badge badge-pill badge-primary  
                 bg-primary"> Image searching API</p>
        
            </form>
            </>
        )
    }
}
