import React from 'react'

export default function ImageList(props) {
    const imagelist = props.image.map((each)=>{
        return <img key={each.id} className='imc rounded'
         src={each.webformatURL} alt='pics'/>
    })

    return ( 
        <div className='main'>
            
            <div className='imagehold'>
            {imagelist}
            </div>
        </div>
    )
}
