import React, { Component } from 'react'
import GetInput from './GetInput'
import axios from 'axios';
import ImageList from './ImageList';

export default class SearchInput extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             photos: []
        }
    }
    
    onSearch = async (entr) =>{
        const response = await axios.get(`https://pixabay.com/api/?key=22987139-01926e150c88f4cdc41a9488b&q=${entr}&image_type=photo`)
        this.setState({photos:response.data.hits})
    }
    render() {
        return (
            <>  
                <GetInput onSearch={this.onSearch}></GetInput>
                <ImageList image={this.state.photos}></ImageList>
            </>
        )
    }
}
