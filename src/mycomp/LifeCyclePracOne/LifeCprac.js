import React, { Component } from 'react'

export default class LifeCprac extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             r: 'render'
        }
    }
    

    componentDidMount(){
        console.log('Component did mount')
    }

    componentDidUpdate(){
        console.log('component did Update')
    }

    click=()=>{
        this.setState({r:'Render updated'})
    }
    render() {
        return (
            <>
              <button onClick={this.click}> Render Update </button>  
                {console.log(this.state.r)}
            </>
        )
    }
}
