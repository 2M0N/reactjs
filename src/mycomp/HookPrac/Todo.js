import React from 'react'
import AddTodo from './AddTodo';
export default function Todo() {

    const [todos, setTodos] = React.useState([ 

        {text:"Sleep Early", id:1},
        {text:"Drink Water", id:2},
        {text:"Go To Shop", id:3},

        // Using array for .map()
    ]);

    // New State 

    const [testarr, setTestarr] = React.useState(0)

    const addTodo = (add) => {
        setTodos([ // Like this.setState() ======

            ...todos, // You have to take old states first! ===
            {text:add, id:Math.random()}

            // ========= Math.random() is'nt ideal here. ======

        ]);
    }

    // =========== Use Effect Hook ==========

    React.useEffect(()=>{
        return console.log("useEffect",todos)
    },[todos])

    React.useEffect(()=>{
        return console.log("useEffect",testarr)
    },[testarr])

    // ========== Button ++1 ==========

    const btnclick = () => {
        setTestarr(testarr+1)
    }
    
    return (
        <div> <button className="mb-3 btn-success" onClick={btnclick}
              > Clicked {testarr} </button>

            <AddTodo addTodo={addTodo}></AddTodo>
            <ul> 
                
                
                {todos.map(each=>{
                    return <li key={each.id}> {each.text} </li>
                })}
                
                {/* {console.log(todos)} */}
            </ul>
        </div>
    )
}
