import React from 'react'

export default function AddTodo({addTodo}) {

    const [inpvalue, setInpvalue] = React.useState('')

    const onInpChang = (e) => {
       
        setInpvalue(e.target.value)
    }

    const onFormsub = (e) => {
        e.preventDefault()
        addTodo(inpvalue)
        setInpvalue('')
    }
    return (
        <div>
            <form onSubmit={onFormsub}>
                <label className='mb-3'> Add New ToDo! </label> <br></br>
                <input className='mb-3 border-info bg-primary
                form-control'  type='text' value={inpvalue}
                onChange={onInpChang} />
            </form>
        </div>
    )
}
