import React, { Component } from 'react'

export default class DemoButton extends Component {
    shouldComponentUpdate(nextProps){
        const { locale: Clocale} = this.props
        const { locale: Nlocale} = nextProps
        // console.log(Clocale); console.log(Nlocale)
        if ( Clocale === Nlocale) {
            return false
        }
        else {
            return true
        }
        
    }


    render() {
         console.log('DemoButton')
         
        const {change,locale} = this.props
      
        
        return (
            <div>
                <button onClick={ () => change(locale)}> Click to Change</button>
                 
            </div>
        )
    }
}
