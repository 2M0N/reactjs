import React, { Component } from 'react'
import DemoButton from './DemoButton'

export default class ClockUpdate extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             clock: new Date(),
             locale : 'en-US'
        }
    }

// if props not need here
    // state = { clock: new Date(), locale : 'en-US'}


    // this.Timer for Unmount
    
    componentDidMount() {
      this.Timer = setInterval( () => this.tick(),1000)
     
    
    }

    // componentWillUnmount() {
    //     clearInterval(this.Timer)
    // }

    tick =()=> {
// setState function makes problems here
        this.setState({
            clock: new Date(),
        });
    }

    HandleClick = (locale) => {
        this.setState({
           locale: locale
        // you can use just parameter here
        //    locale
        })
    }

    render() {
             
        //  const {local} = this.props
        const {locale} = this.state
    //    console.log( 'parent',this.state.locale)
    //    let clockb;

        // if (locale ==='en-US') {
        //     clockb = (
        //         <DemoButton change={this.HandleClick} locale={'bn-BD'}> </DemoButton>
        //     )
        // }
        // else {
        //     clockb = (<DemoButton change={this.HandleClick} locale={'en-US'}> </DemoButton> )
        // }





        return (
            <div>
            
{this.state.clock.toLocaleTimeString(this.state.locale)}
<br></br>
{/* // use bind or arrow function with parameter */}
{/* <button onClick={this.HandleClick.bind(this,'bn-BD')}>bn-BD</button> */}
{/* <button onClick={ () => this.HandleClick('bn-BD')}>bn-BD</button> */}


{/* you can use bind or arrow */}
{/* <DemoButton change={this.HandleClick} locale={'bn-BD'}> </DemoButton> */}

{/* inline verify */}
{locale === 'en-US' ? ( <DemoButton change={this.HandleClick} locale={'bn-BD'}> </DemoButton>)
: ( <DemoButton change={this.HandleClick} locale={'en-US'}> </DemoButton>)}
            </div>
        )
    }
}
