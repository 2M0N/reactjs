import React, { Component } from 'react'

export default class HoverTwo extends Component {

    // constructor(props) {
    //     super(props)
    
    //     this.state = {
    //          count: 0
    //     }
    // }

    // hoverInc = () => {
    //     this.setState(p=>{
    //         return{
    //             count:p.count + 1
    //         }
    //     })
    // }
    
    render() {
        const {count,inCcount} = this.props
        return (
            <div>
                <h2 onMouseOver={inCcount}>
                     {count} times hovered</h2>
            </div>
        )
    }
}
