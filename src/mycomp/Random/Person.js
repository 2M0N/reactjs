import React from 'react'

export default function Person({p}) {
    return (
        <div>
             <h2> I'm {p.name}. I'm {p.age}. I know {p.skill} </h2>
             
        </div>
    )
}
