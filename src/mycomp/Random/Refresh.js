import React, { Component } from 'react'

class Refresh extends Component{

    constructor(){
        super() 
        this.Handle = this.Handle.bind(this)
    }

    Handle() {
        this.forceUpdate()
    }

render(){
return(
<div>
<button onClick = {this.Handle}> Refresh</button>
        <h1>{Math.random()}</h1>
    </div>

        )}}

export default Refresh