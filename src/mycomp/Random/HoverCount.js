import React, { Component } from 'react'
import withComp from './withCompHOC'

    class HoverCount extends Component {

    // state = {
    //     count: 0
        
    // }


    // handleClick = () => {
    //     this.setState((p)=> {
    //         return {
    //             count: p.count +1
    //         }
    //     })
    // }
    render() {

        // ***** ideal way! *****
        // const {count,handleClick} = this.props
        return (
            <div>
                <h2 onMouseOver={this.props.handleClick}> Hoverd {this.props.count} times </h2>
            </div>
        )
    }
}
export default withComp(HoverCount,3)