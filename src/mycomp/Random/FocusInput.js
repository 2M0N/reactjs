import React, { Component } from 'react'
import InputRef from './InputRef'

export class FocusInput extends Component {

    constructor(props) {
        super(props)

        this.compRef = React.createRef()
    }

    oncHandler = () => {
        this.compRef.current.focusInput()
        }
    componentDidMount(){
        console.log(this.compRef)
    }
    render() {
        return (
            <div>
                <InputRef ref={this.compRef} />
                <button onClick={this.oncHandler}> Focus Input</button>
            </div>
        ) 
    }
}

export default FocusInput
