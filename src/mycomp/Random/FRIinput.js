import React from 'react'

// ============= with class comp =========

// export default class FRIinput extends Component {

//     constructor(props) {
//         super(props)
    
//         this.InpRef = React.createRef()
//     }

//     focusinp = () => [
//         this.InpRef.current.focus()
//     ]
    
//     render() {
//         return (
//             <div>
//                 <input type='text' ref={this.InpRef} />
//             </div>
//         )
//     }
// }

// ======= Func Comp ========

const FRIinput = React.forwardRef( (props,ref) => {
    return (
        <div>
        <input type='text' ref={ref} />
        </div>
        )
})

export default FRIinput

