import React, { Component } from 'react'
import { UC } from './UserContext'

export default class CompF extends Component {
    render() {
        
        return (
            <UC.Consumer>
                {
                    (valuep) => <div> Hello {valuep} </div>
                }
            </UC.Consumer >
        )
    }
}
