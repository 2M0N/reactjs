import React from 'react'

const withComp = (Orginal,incNum) => {
    class NewComp extends React.Component {

        constructor(props) {
            super(props)
        
            this.state = {
                 count: 0
            }
        }
        

    handleClick = () => {
            this.setState((p)=> {
                return {
                    count: p.count + incNum
                }
            })
        }

        render() {
            return  <Orginal count={this.state.count} 
            handleClick={this.handleClick}
            {... this.props} />
        }
    }
    return NewComp
}
export default withComp