import React, { Component } from 'react'

export default class Aform extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             title: 'React',
             text : 'Area',
             library: 'Angular',
             check: true
        }
    }
// #########  Form Submit #######

    submitHandler = (e) => {
        e.preventDefault()
        const {title, text, library, check} = this.state
        console.log(title, text, library, check)
    }

handle = (e) => {

// ######## The Smart Way #######

    // this.setState({
    //     [e.target.name]: e.target.value
    // })

// ###############################


    if (e.target.type === 'text') {
        this.setState({
            title: e.target.value
        })
    }
    else if (e.target.type === 'textarea') {
        this.setState({
            text: e.target.value
        })
    }

    else if (e.target.type === 'select-one') {
        this.setState({
            library: e.target.value
           
        })
    }

    if (e.target.type === 'checkbox') {
        this.setState({
            check: e.target.checked
        })
    }
    else {
        console.log(e.target.type)
    }
}

// ####################################
    
    render() {
        const {title, text, library, check} = this.state
        return (
            <div>
                <form onSubmit={this.submitHandler}> <br></br>

        {/* ******* Input ******** */}
                <input
                value={title} onChange={this.handle}></input>

                <br></br> <br></br>

        {/* ******* Text area ******** */}
            <textarea type='textarea' 
            value={text} onChange={this.handle} > </textarea>
                <br></br> <br></br>

        {/* ******* Select Options ******** */}

            <select  value={library} onChange={this.handle} >
                 <option value={'React'} > React</option>
                 <option value={'Angular'}> Angular</option>
             </select>
             <br></br> <br></br>

        {/* ******* Checkbox Select ******** */}
                <input type="checkbox" checked={check} 
                onChange={this.handle} />
         <br></br> <br></br>

        {/* ******* Submit ******** */}
                <input type='submit' />
                
                    
                </form>
            </div>
        )
    }

}
