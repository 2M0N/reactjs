import React from 'react'
import Person from './Person'

export default function NameList() { 
    const persons = [
    {id: 1,name: 'Sumon',age: 23,skill: 'JS'},
    {id: 2,name: 'Jahid',age: 24,skill: 'PHP'},
    {id: 3,name: 'Samil',age: 32,skill: 'Python'}
]
    
const personList = persons.map((p,index) => { 
return (
    
<Person key={p.id} p={p}/>

)})
    return (
        <div>
            {personList}
        </div>
    )
}
