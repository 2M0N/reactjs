import React from 'react'


export default function TempInput(props) {

    const scalemng = {
        c: 'Celsius',
        f: 'Fahrenheit'
    }

    const {temp,onTemp,scale} = props

    return (
        <div>
             <div>
                <fieldset>
                    <legend>Type The Temp {scalemng[scale]} </legend>
                    <input type="number" value={temp} onChange={(e)=> onTemp(e,scale)} />
                </fieldset>
            </div>
        </div>
    )
}
