import { Component } from 'react'

export default class Jsonlist extends Component {
    render() {

        const arrlist = [
            {
                city: 'Feni',
                zip: '4444'
            },
            {
                city: 'Kumilla',
                zip: '6666'
            },
            {
                city: 'Dhaka',
                zip: '5555'
            }
        ]

        const datalist = arrlist.map(data=>{
          return  <li> {data.city} zip = {data.zip} </li>
        })
        return (
            <div>
                <ul> {datalist} </ul>
            </div>
        )
    }
}
