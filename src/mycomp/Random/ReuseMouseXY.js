import React, { Component } from 'react'

export default class ReuseMouseXY extends Component {
    render() {

        return (
            <div style={{ height: '1000px'}} onMouseMove={this.props.handleMouse}>
            <p style={{color:'red'}}> X = {this.props.x}, Y = {this.props.y} </p>
                
            </div>
        )
    }
}
