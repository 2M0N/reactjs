import React, { Component } from 'react'
import CompF from './CompF'
import { UC } from './UserContext'

export default class CompE extends Component {
    static contextType = UC
    render() {
        
        return ( <div>
            Hello {this.context} <CompF />
        </div> )
    }
}
