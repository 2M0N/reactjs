import React from 'react'

class MouseXY extends React.Component {

    constructor(props) {
        super(props)
        this.state = {x:0, y:0}
    }
    
    handleMouse = (event) => {
        this.setState({
            x: event.clientX,
            y: event.clientY
        })
    }

    render() {
        return (
            <div>
                {this.props.render(this.state.x,this.state.y,this.handleMouse)}
            </div>
        )
    }
}
export default MouseXY