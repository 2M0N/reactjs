import React, { Component } from 'react'

export default class ErrBound extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            hasErr: false
        }
    }
    
    static getDerivedStateFromError(error) {
        return {
            hasErr: true   
        }

    }
    render() {
        if (this.state.hasErr) {
            return <p> 404 error {console.log(this.state.hasErr)} </p>
        }
        return this.props.children        
    }
}
