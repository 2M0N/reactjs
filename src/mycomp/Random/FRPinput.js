import React, { Component } from 'react'
import FRIinput from './FRIinput'

export default class FRPinput extends Component {

    constructor(props) {
        super(props)

        this.inputRef = React.createRef()
    }

    onChandler = () => {
        this.inputRef.current.focus()
    }
    
    render() {
        return (
            <div>
                
                <FRIinput ref={this.inputRef} />
                <button onClick={this.onChandler}> Focus Input </button>
            </div>
        )
    }
}
