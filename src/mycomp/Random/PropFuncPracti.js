import React, { Component } from 'react'

export default class PropFuncPracti extends Component {
    render() {
        return (
            <div>
                {this.props.render(true)}
            </div>
        )
    }
}
