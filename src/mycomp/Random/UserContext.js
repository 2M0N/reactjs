import React from 'react'

export const UC = React.createContext()

// ****** other way ******

// export const UCProvider = UC.Provider
// export const UCConsumer = UC.Consumer
