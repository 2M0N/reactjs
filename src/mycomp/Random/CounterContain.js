import React, { Component } from 'react'

export default class CounterContain extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             count: 0
        }
    }

    inCcount = () => {

        this.setState(p =>{
        return { count: p.count + 1}
        })
    }

    render() {
        return (
            <div>
                {this.props.render(this.state.count,this.inCcount)}
            </div>
        )
    }
}
