import React from 'react'

export default function Hero({HeroName}) {

    if (HeroName === 'Joker') {
    throw new Error('Not a Hero!')
    }
    return (
        <div>
            {HeroName}
        </div>
    )
}
