import React from 'react'
import {Component} from 'react'

class Cond extends Component{

    constructor(){
        super()

        this.state={
            login:false
        }
    }

    render(){

    //     if (this.state.login==false) {
    //         return (<button> Logout</button>)
    // }
    //     else {
    //         return (<button> Log in</button>)
    //     }
    
    return (
        this.state.login? 
        (<h1> YES!</h1>) :
        (<h1> NO!</h1>)
    )
    }

}

export default Cond