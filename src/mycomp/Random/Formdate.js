import React, { Component } from 'react'

export default class Formdate extends Component {

    constructor(){
        super()

        this.state={
            // fname:"",
            // lname:"",
            // email:"",
            // numb: ""
        }}
    
    eLa=(event)=>{
        const names = event.target.name
        const value = event.target.value

        this.setState({[names]:value})

        if (names==='fname'){
            const np= /^([a-zA-Z]){2,30}$/;
            if(!np.test(value)){
                this.setState({fname:'Not valid'})
            }
            
        }

        if (names==='lname'){
            const np= /^([a-zA-Z]){2,30}$/;
            if(!np.test(value)){
                this.setState({lname:'Not valid'})
            }

        }

        if (names==='email'){
            const em = /^[^@ ]+@[^@ ]+\.[^@ ]+$/   
            if(!em.test(value)){
                this.setState({email:"Not valid"})
            }

        }

        if (names==='numb'){
            if (!Number(value)) {
                this.setState({numb:"Not valid"})
            }

        }
    }    
        
    render() {
        return (
        <div>
        <p> First Name: {this.state.fname}</p>
        <p> Last Name: {this.state.lname}</p>
        <p> Your Email: {this.state.email}</p>
        <p> Mobile NO: { this.state.numb}</p>
                <form>
<input placeholder="First Name" onChange={this.eLa}  type="text" name="fname"/> <br></br>
<input placeholder="Last Name" onChange={this.eLa} type="text" name="lname"/> <br></br>
<input placeholder="Your Email" onChange={this.eLa} type="text" name="email"/> <br></br>
<input placeholder="Mobile No" onChange={this.eLa} type="text" name="numb"/> <br></br>
<input type="submit" name="submit" value='Save'/>

                </form>
            </div>
        )
    }
}
