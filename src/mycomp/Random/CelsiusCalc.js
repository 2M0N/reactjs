import React, { Component } from 'react'
import { converter, toCelsius, toFahrenheit } from '../lib/converter'
import Boilling from './Boilling'
import TempInput from './TempInput'

export default class CelsiusCalc extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             temp: '',
             scale: ''
        }
    }

    onTemp = (e,scale) => {
        this.setState({
            temp: e.target.value,
            scale,
        })
    }

  
    render() {
        
        const {temp,scale} = this.state
        const celsius = scale === 'f' ? converter(temp, toCelsius) : temp
        const fahrenheit = scale === 'c' ? converter(temp, toFahrenheit) : temp

        return (
            <div>

            <TempInput temp={celsius} 
            onTemp={this.onTemp} scale={'c'} ></TempInput>

            {/* ############################################## */}

            <TempInput temp={fahrenheit} 
            onTemp={this.onTemp} scale={'f'} ></TempInput>

            {/* ############################################## */}

            <Boilling celsius={temp}></Boilling>
            </div>
        )
    }
}
