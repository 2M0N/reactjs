import React, { Component } from 'react'
import withComp from './withCompHOC';

    class ClickCountp extends Component {

    // state = {
    //     count: 0
       
    // }

    // handleClick = () => {
    //     // Use updater function when new state is derived from old
        
    //     // ***** do with funtion ******
    //     // this.setState(prev => ({ count: prev.count + 1 }));

    //     // ******* That's another way to do ********
    //     this.setState({ count: this.state.count + 1 })
    //   };

    

    render() {
        const {count,handleClick} = this.props
        return (
            <div>
                <button onClick={handleClick} >
                 {this.props.name} Clicked {count} times</button>
            </div>
        )
    }
}
export default withComp(ClickCountp,4)
