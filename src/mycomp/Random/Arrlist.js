import React, { Component } from 'react'

export default class Arrlist extends Component {

    function = (data) => {
        return <li> {data+2} </li>
     }
    render() {
        const arr = ['1','2','3','4','5']
        const list = arr.map(this.function)
        return (
        
            <div>
                <ul> {list} </ul>
            </div>
        )
    }
}
