import { useHook1 } from './useHook1'
export const CheckScreenSize = () => {

    const hook = useHook1(555);
    return (
        <div>
          
            {hook ? "Small" : "Large"}
        </div>
    )
}
