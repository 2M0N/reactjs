import { useCallback,useEffect,useState } from 'react'

export const useHook1 = (screenSize) => {

    const [onSmallScreen, setonSmallScreen] = useState(false)
    
    const checkScreenSize = useCallback(()=>{
        setonSmallScreen(window.innerWidth < screenSize);
        console.log("checkScreenSize", screenSize, window.innerWidth)
    },[screenSize])

    // const checkScreenSize = () => {
    //     setonSmallScreen(window.innerWidth < screenSize);
    //     console.log("checkScreenSize")
    // }

    useEffect(()=> {
        checkScreenSize()
        window.addEventListener('resize',checkScreenSize)
        console.log('useEffect')
    },[checkScreenSize])

    // useCallback(()=> {
    //     checkScreenSize()
    //     window.addEventListener('resize',checkScreenSize)
    //     console.log('useEffect')
    // },[checkScreenSize])
    return onSmallScreen
}
