import { useState } from 'react'
import ShowList from './ShowList'
import './TodoApp.css' // Full CSS

export const TodoApp = () => {
    // Item values
    const [items, setitems] = useState([])

    // Current Input Value
    const [inpval, setinpval] = useState('')
    
    // Add new Item and Clear Current inp Value
    const btn = () => {
        setinpval([''])
        setitems([...items,inpval])
        
       
    }

    // It's just returns whtat doesn't match with (i)
    const deleteArray = (i) => {
        setitems(items.filter((arr,index)=> index !== i ))
       
    }
    return ( 
        <div className='main'>
            <div className='centerDiv'> 

            {/* Header */}
            <h2 className='heading'> ToDo App </h2> <br></br>
            
           
            {/* Input and Add button  */}
            <div className='additems'> 

            <input className='inp' placeholder='Add new items...'
            onChange={(e)=> setinpval(e.target.value)}
            value={inpval} />

            {/* Add taks Button */}
            <button className='plusicon' onClick={btn}> + </button>

            </div> <br/> 

            {/* Show Lists  (Passing two props) */}
            <ShowList  items={items} del={deleteArray} />
          
             {/* Footer Texts */}
            <div className='footer'> Total {items.length} tasks listed </div>
            </div>
        </div>
    )
}
