import React from 'react'

export default function ShowList({del,items}) {
    return (
        <div className='ulhold'>
            {items.map((each,i) => {
                return (
                   
                    <ul> 
                       {/* Delete Button  */}
                    <button onClick={() => del(i)} 
                    className='deleteIcon' > × </button> 

                       {/* Added Items */}
                    <li key={i}> {each} </li>

                    </ul>
                    
                ) 
            })}
        </div>
    )
}
